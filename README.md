# GaussianCovariances

Python Module to compute the covariance of 
two-point clustering statistics (power-spectrum and two-point correlation multipoles), 
based on the work described in Grieb et al. 2016 (https://arxiv.org/abs/1509.04293).

## Installation

You can install this package in two ways:

* Using pip:

    <pre> pip install gaussiancovariance </pre>

* From this repository
    - Clone the repository:

       <pre> git clone https://gitlab.com/veropalumbo.alfonso/gaussiancovariance/ </pre>
    - Install:

        <pre> pip install .</pre>

# Usage

Please refer to the demo folder for basic examples


    

